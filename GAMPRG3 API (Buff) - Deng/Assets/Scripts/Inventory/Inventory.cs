﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> items = new List<Item>();
    public int slots=10;
    public BuffReceiver character;
    public GameObject buffManager;
    public InventoryUI inventoryUI;

    private void Awake()
    {
        inventoryUI.SetInventory(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        slots = 10;
        //AddItem(new Item { name = "sword", description = "A regular sword",amount=1,maxStack=3, usable = true, isStackable = true });
        

        //AddItem(new Item { name = "dagger", description = "A tiny dagger",amount=1,maxStack=1, usable = true, isStackable = true });
        //AddItem(new Item { name = "dagger", description = "A tiny dagger",amount=1,maxStack=1, usable = true, isStackable = true });

        //AddItem(new Item { name = "bottle", description = "A bottle",amount=1, usable = false, isStackable = false });
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    //UseItem(new Item { name = "sword", description = "A regular sword", amount = 1, maxStack = 3, usable = true, isStackable = true });
        //    UseItem(new Item { name = "bottle", description = "A bottle", amount = 1, usable = false, isStackable = false });
            
        //}
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    AddItem(new Item { name = "sword", description = "A regular sword", amount = 1, maxStack = 3, usable = true, isStackable = true });
        //    Debug.Log("Added");
        //}
        //if (Input.GetKeyDown(KeyCode.U))
        //{
        //    UseItem(items[0]);
        //    Debug.Log("Used"+items[0].name);
        //}
        if (Input.GetKeyDown(KeyCode.M))
        {
            UseItem(items[0]);
            Debug.Log("Used "+items[0].name);
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            UseItem(items[1]);
            Debug.Log("Used "+items[1].name);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            UseItem(items[2]);
            Debug.Log("Used " + items[2].name);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            UseItem(items[3]);
            Debug.Log("Used " + items[3].name);
        }

    }

    public void AddItem(Item itemToAdd)
    {
        if (items.Count<slots)
        {
            if (itemToAdd.isStackable)
            {
                CheckStack(itemToAdd);
            }
            else
            {
                items.Add(itemToAdd);
            }
        }
        else
        {
            Debug.Log("Inventory Full");
        }
        if (!itemToAdd.usable)
        {
            UseItem(itemToAdd);
        }
        
    }

    public void CheckStack(Item itemToCheck)
    {
        bool itemAlreadyInInventory = false;
        foreach (Item item in items)
        {
            if (item.name == itemToCheck.name && !item.stackIsMaxed)
            {
                if (item.amount == itemToCheck.maxStack - 1)
                {
                    item.stackIsMaxed = true;
                }
                item.amount += itemToCheck.amount;
                itemAlreadyInInventory = true;
            }
        }
        if (!itemAlreadyInInventory)
        {
            items.Add(itemToCheck);
        }
    }

    public void RemoveItem(Item itemToRemove)
    {
        if (itemToRemove.isStackable)
        {
            Item itemInInventory = null;
            bool stackDecreased=false;
            foreach(Item item in items)
            {
                if (item.name==itemToRemove.name&&!stackDecreased)
                {
                    item.amount -= itemToRemove.amount;
                    itemInInventory = item;
                    stackDecreased = true;
                }
            }
            if (itemInInventory !=null&&itemInInventory.amount<=0)
            {
                items.Remove(itemInInventory);
            }
        }
        else
        {
            items.Remove(itemToRemove);
        }
    }

    public void UseItem(Item itemToUse)
    {
        //if (itemToUse.usable)
        //{
        //    //Apply Effects
        //    UsableItem usableItem = (UsableItem)itemToUse;
        //    usableItem.ApplyBuff(character);
        //    RemoveItem(itemToUse);
        //    Debug.Log("Removed");
        //}
        UsableItem usableItem = (UsableItem)itemToUse;
        usableItem.ApplyBuff(character,buffManager);
    }

    public List<Item> GetItemList()
    {
        return items;
    }
}

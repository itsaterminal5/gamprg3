﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Item : ScriptableObject
{
    public string name;
    public string description;
    public bool usable;
    public int amount;
    public bool isStackable;
    public bool stackIsMaxed;
    public int maxStack /*= 250*/;
    public Sprite icon;



    //public Item(string _name,string _description,bool _usable,bool _isStackable,int _stack, int _maxStack )
    //{
    //    this.name = _name;
    //    this.description = _description;
    //    this.usable = _usable;
    //    this.isStackable = _isStackable;
    //    this.stack = _stack;
    //    this.maxStack = _maxStack;
    //}


}

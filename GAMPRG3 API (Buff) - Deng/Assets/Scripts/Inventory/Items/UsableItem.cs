﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class UsableItem : Item
{
    public bool consumable;
    public bool hasRandomEffect;
    public List<Buff> buffs = new List<Buff>();

    public void ApplyBuff(BuffReceiver receiver,GameObject source)
    {
        if (!hasRandomEffect)
        {
            foreach (Buff buff in buffs)
            {
                //Debug.Log(buff.name);
                //receiver.ApplyBuff(Buff.Instantiate(buff), source);
                //BuffManager.instance.AddMight();
                BuffManager.instance.receiver.ApplyBuff(Buff.InstantiateBuff(buff), source);
            }
        }
        else
        {
            int randomizer = Random.Range(1, 100);
            if (randomizer<=20)
            {
                BuffManager.instance.receiver.ApplyBuff(Buff.InstantiateBuff(buffs[0]), source);
            }
            else
            {
                BuffManager.instance.receiver.ApplyBuff(Buff.InstantiateBuff(buffs[1]), source);
            }

        }
        
    }

    
    
}

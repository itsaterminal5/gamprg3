﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class BuffListController : MonoBehaviour {
    public BuffReceiver Actor;
    public BuffElementController ElementPrefab;

    private List<BuffElementController> activeElements = new List<BuffElementController>();

    private void Start()
    {
        Assert.IsNotNull(Actor, "Requires Actor to be set");

        // Refresh list when buffs change
        Actor.BuffActivated.AddListener(b => Refresh());
        Actor.BuffDeactivated.AddListener(b => Refresh());

        Refresh();
    }

    public void Refresh()
    {
        // Destroy all active elements
        foreach (BuffElementController element in activeElements)
        {
            Destroy(element.gameObject);
        }
        activeElements.Clear();

        // Get unique buffs only based on id
        var uniqueBuffs = Actor.Buffs
            .GroupBy(b => b.Id)
            .Select(b => b.First())
            .ToList();

        // Instantiate element per active unique buff
        int elementIndex = 0;
        foreach (Buff buff in uniqueBuffs)
        {
            BuffElementController element = Instantiate(ElementPrefab);
            element.Buff = buff;

            RectTransform rect = element.GetComponent<RectTransform>();
            rect.SetParent(gameObject.transform);
            rect.SetAsLastSibling();
            rect.localScale = new Vector3(1, 1, 1);
            rect.localPosition = new Vector3(0, elementIndex * rect.rect.height * -1, rect.localPosition.z);

            activeElements.Add(element);
            elementIndex++;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public BuffReceiver receiver;
    public List<Text> stats = new List<Text>();
    public List<Text> bonusStats = new List<Text>();
    public Text health;
    float playerHealth;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        SetStatText();
        SetBonusStatText();
        playerHealth = receiver.GetComponent<Health>().CurrentHp;
        health.text = Mathf.RoundToInt(playerHealth).ToString() ;
    }

    public void SetStatText()
    {
        stats[0].text = "Power:" + receiver.GetComponent<Stats>().StatsBase.Power;
        stats[1].text = "Precision:" + receiver.GetComponent<Stats>().StatsBase.Precision;
        stats[2].text = "Toughness:" + receiver.GetComponent<Stats>().StatsBase.Toughness;
        stats[3].text = "Vitality:" + receiver.GetComponent<Stats>().StatsBase.Vitality;

    }
    public void SetBonusStatText()
    {
        bonusStats[0].text = "+ " + receiver.GetComponent<Stats>().StatsAddBonus.Power;
        bonusStats[1].text = "+ " + receiver.GetComponent<Stats>().StatsAddBonus.Precision;
        bonusStats[2].text = "+ " + receiver.GetComponent<Stats>().StatsAddBonus.Toughness;
        bonusStats[3].text = "+ " + receiver.GetComponent<Stats>().StatsAddBonus.Vitality;

    }


}

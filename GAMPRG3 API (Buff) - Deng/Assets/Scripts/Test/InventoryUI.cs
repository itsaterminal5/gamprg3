﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    private Inventory inventory;
    private Transform itemSlotContainer;
    private Transform itemSlotTemplate;

    private void Awake()
    {
        itemSlotContainer = transform.Find("itemSlotContainer");
        itemSlotTemplate = itemSlotContainer.Find("itemSlotTemplate");

    }
    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;
        RefreshInventory();
    }
    private void RefreshInventory()
    {
        int x = 0;
        int y = 0;
        float itemSlotCellSize = 100f;
        foreach(Item item in inventory.GetItemList())
        {
            RectTransform itemSlotRecTrans = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRecTrans.gameObject.SetActive(true);
            itemSlotRecTrans.anchoredPosition = new Vector2(x * itemSlotCellSize, y * itemSlotCellSize);
            Image image = itemSlotRecTrans.Find("icon").GetComponent<Image>();
            image.sprite = item.icon;
            x++;
            if (x>4)
            {
                x = 0;
                y++;
            }
        }
    }
}

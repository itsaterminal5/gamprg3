﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBuffer : MonoBehaviour {

    public BuffReceiver Actor;
    public Buff[] Buffs;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1)) Actor.ApplyBuff(Buff.InstantiateBuff(Buffs[0]), gameObject);
        if (Input.GetKeyDown(KeyCode.Alpha2)) Actor.ApplyBuff(Buff.InstantiateBuff(Buffs[1]), gameObject);
        if (Input.GetKeyDown(KeyCode.Alpha3)) Actor.ApplyBuff(Buff.InstantiateBuff(Buffs[2]), gameObject);
    }
}

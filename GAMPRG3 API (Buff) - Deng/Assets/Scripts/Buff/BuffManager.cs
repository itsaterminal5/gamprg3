﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffManager : MonoBehaviour
{
    public BuffReceiver receiver;
    public static BuffManager instance;
    public List<Buff> buffs = new List<Buff>();
    //public Stats receiverStats;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        receiver.GetComponent<Stats>().StatsBase.Power = 10;
        receiver.GetComponent<Stats>().StatsBase.Precision = 10;
        receiver.GetComponent<Stats>().StatsBase.Toughness = 10;
        receiver.GetComponent<Stats>().StatsBase.Vitality = 10;
    }
    void Start()
    {
        //receiver.GetComponent<Stats>().Vitality{ };
        //receiverStats = receiver.GetComponent<Stats>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) receiver.ApplyBuff(Buff.InstantiateBuff(buffs[0]), gameObject);
        if (Input.GetKeyDown(KeyCode.Alpha2)) receiver.ApplyBuff(Buff.InstantiateBuff(buffs[1]), gameObject);
        if (Input.GetKeyDown(KeyCode.Alpha3)) receiver.ApplyBuff(Buff.InstantiateBuff(buffs[2]), gameObject);
        if (Input.GetKeyDown(KeyCode.Alpha4)) receiver.ApplyBuff(Buff.InstantiateBuff(buffs[3]), gameObject);

    }

    public void AddPower()
    {
        receiver.GetComponent<Stats>().StatsAddBonus.Power += 100;
    }


    public void AddVitality()
    {
        receiver.GetComponent<Stats>().StatsAddBonus.Vitality += (receiver.GetComponent<Stats>().StatsBase.Vitality / 2); 
    }
    public void ResetVitality()
    {
        receiver.GetComponent<Stats>().StatsAddBonus.Vitality = 0; 
    }
    public void PoisonHealth()
    {
        receiver.GetComponent<Health>().CurrentHp -= (receiver.GetComponent<Health>().CurrentHp * 0.02f);
        //receiver.GetComponent<Health>().CurrentHp -= 1;

    }

    public void RegenHealth()
    {
        receiver.GetComponent<Health>().CurrentHp += (receiver.GetComponent<Health>().CurrentHp*0.05f);
        //receiver.GetComponent<Health>().CurrentHp += 1;

    }

    public void CurePoison()
    {
        receiver.RemoveBuff("poison");
    }

    





}

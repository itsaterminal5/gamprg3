using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Buff")]
public class Buff : ScriptableObject
{
    [Header("Identifier")]
    /// <summary>
	/// The identifier of this type of buff. Buff of the same identifier will be handled based on BuffApplication.
	/// </summary>
    public string Id;
    /// <summary>
	/// Display name for the buff.
	/// </summary>
    public string Name;
    /// <summary>
    /// Icon used by this buff type
    /// </summary>
    public Sprite Icon;

    [Header("Application")]
    /// <summary>
    /// Mode of how the buff is applied.
    /// </summary>
    public BuffApplication Application;
    /// <summary>
    /// Maximum number of stacks of the same buff a receiver can have. Only relevant if Application is StackingIntensity.
    /// Set to 0 for no stack limit
    /// </summary>
    public int MaxStacks;
    /// <summary>
    /// Determines how fast this buff ticks every interval (seconds). This is useful for debuffs that damages a unit over time.
    /// Set to 0 if this buff doesn't need to take effect every tick
    /// </summary>
    public float TickInterval;

    [Header("Instance Configuration")]
    public int Stacks;
    public float Duration;

    /// <summary>
    /// The unit who applied the buff. Set this if needed.
    /// </summary>
    public GameObject Source { get; private set; }
    /// <summary>
    /// The unit whom this buff is applied.
    /// </summary>
    public BuffReceiver Target { get; private set; }
    public bool Active { get; private set; }
    public float TimeElapsed { get; private set; }
    public float TimeRemaining { get { return Duration - TimeElapsed; } }
    public float DurationNormalized { get { return Duration > 0 ? TimeElapsed / Duration : 1.0f; } }
    /// <summary>
    /// Checks if buff was already instantiated from an asset
    /// </summary>
    /// <value><c>true</c> if instantiated; otherwise, <c>false</c>.</value>
    public bool Instantiated { get { return Asset != null; } }
    /// <summary>
    /// This is set upon cloning the buff from an asset
    /// </summary>
    /// <value>The asset.</value>
    public Buff Asset { get; private set; }

    // Events
    public BuffActivated Activated = new BuffActivated();
    public BuffDeactivated Deactivated = new BuffDeactivated();
    public BuffTicked Ticked = new BuffTicked();
    public BuffExpired Expired = new BuffExpired();
    public BuffStacksChanged StacksChanged = new BuffStacksChanged();

    #region Overridables
    protected virtual void OnActivate() { }
    protected virtual void OnDeactivate() { }
    protected virtual void OnBuffUpdate(float dt) { }
    protected virtual void OnTick() { }
    /// <summary>
    /// Determines which buff gets prioritized when stacked
    /// By default, old buff with lesser time remaining than the new buff will be overriden
    /// </summary>
    /// <param name="other">Buff to be stacked (new)</param>
    /// <returns>True if buff should be overriden</returns>
    protected virtual bool OnOverriding(Buff other) { return TimeRemaining > other.TimeRemaining; }
    protected virtual void OnStacksChanged(int prevStacks) { }
    #endregion

    public static T InstantiateBuff<T>(T source)
        where T : Buff
    {
        Assert.IsFalse(source.Instantiated, "Source must be an asset object");

        T clone = Object.Instantiate<T>(source);
        clone.Asset = source;
        return clone;
    }

    public Buff CloneBuff() 
    {
        return InstantiateBuff(this);
    }

    public void Activate(GameObject source, BuffReceiver target)
    {
        Assert.IsFalse(Active, "Buff is already activated");
        Assert.IsTrue(Instantiated, "Buff must be instantiated from an asset before activating");
        Assert.IsNotNull(target);

        Active = true;
        Source = source;
        Target = target;

        OnActivate();
        Activated.Invoke(this);
        Target.BuffActivated.Invoke(this);

        Target.StartCoroutine(UpdateTask());
        if (TickInterval > 0) Target.StartCoroutine(TickTask());
    }

    IEnumerator UpdateTask()
    {
        int previousStacks = 0;

        while (Active)
        {
            OnBuffUpdate(Time.deltaTime);

            // Buff timer
            if (Duration > 0)
            {
                if (TimeElapsed < Duration)
                {
                    TimeElapsed += Time.deltaTime;
                }
                else
                {
                    Expired.Invoke(this);
                    Target.BuffExpired.Invoke(this);
                    Deactivate();
                }
            }

            // Stack monitoring
            if (Application == BuffApplication.StackingIntensity && previousStacks != Stacks)
            {
                OnStacksChanged(previousStacks);
                StacksChanged.Invoke(this, previousStacks);
                Target.BuffStacksChanged.Invoke(this, previousStacks);
            }
            previousStacks = Stacks;

            yield return null;
        }
    }

    IEnumerator TickTask()
    {
        while (Active)
        {
            yield return new WaitForSeconds(TickInterval);
            Tick();
        }
    }

    public void Deactivate()
    {
        bool prevActivated = Active;
        Active = false;

        if (prevActivated)
        {
            OnDeactivate();
            Deactivated.Invoke(this);
            Target.BuffDeactivated.Invoke(this);
            TimeElapsed = 0;
        }

        Destroy(this);
    }

    public bool ShouldOverride(Buff other)
    {
        Assert.IsTrue(other.Id == Id, "ShouldOverride should only be called on buff of the same identifier");
        Assert.IsTrue(other != this, "Cannot be the same buff");

        return OnOverriding(other);
    }

    protected void Tick()
    {
        OnTick();
        Ticked.Invoke(this);
        Target.BuffTicked.Invoke(this);
    }

    public override string ToString()
    {
        return Name + "[" + Id + "]: Application = " + Application + " Max Stacks = " + MaxStacks + " Tick Interval = " + TickInterval + "s";
    }
}

public enum BuffApplication {
    Unique,             // Applies the buff if it doesn't exist yet. Unique buffs cannot be overridden no matter what.
    Override,           // Replaces the old buff of the same type based on buff overriding rules (default = buff with higher time remaining will override the other)
    StackingIntensity,  // Keeps a stack count which increases the intensity depending on the effect of the buff.
    StackingDuration    // Stacks the duration of an existing buff
}

#region Events
public class BuffActivated : UnityEvent<Buff> {}
public class BuffDeactivated : UnityEvent<Buff> {}
public class BuffExpired : UnityEvent<Buff> {}
public class BuffTicked : UnityEvent<Buff> {}
/// <summary>
/// Buff stacks changed.
/// 2nd param = previous stacks
/// </summary>
public class BuffStacksChanged : UnityEvent<Buff, int> {}
#endregion
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Buffs/Might")]
public class Might : Buff
{
    // Start is called before the first frame update
    void Start()
    {
        MaxStacks = 10;
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnActivate()
    {
        Debug.Log("Might is here");
        BuffManager.instance.AddPower();
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Buffs/Vigor")]
public class Vigor : Buff
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnActivate()
    {
        Debug.Log("Vigor boi");
        BuffManager.instance.AddVitality();
    }
    protected override void OnDeactivate()
    {
        BuffManager.instance.ResetVitality();
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buffs/Cure")]
public class Cure : Buff
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    protected override void OnActivate()
    {
        Debug.Log("Cured");
        BuffManager.instance.CurePoison();
    }
    
}

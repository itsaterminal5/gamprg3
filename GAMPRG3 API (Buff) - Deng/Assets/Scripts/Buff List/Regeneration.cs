﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buffs/Regeneration")]
public class Regeneration :Buff
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnActivate()
    {
        
        
    }
    protected override void OnTick()
    {
        Debug.Log("Regen");
        BuffManager.instance.RegenHealth();
    }

}

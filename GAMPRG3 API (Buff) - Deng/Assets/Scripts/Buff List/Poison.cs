﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Buffs/Poison")]
public class Poison : Buff
{
    // Start is called before the first frame update
    void Start()
    {
        MaxStacks = 5;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnActivate()
    {
        
    }
    protected override void OnTick()
    {
        Debug.Log("Poison");
        BuffManager.instance.PoisonHealth();

    }
    
}

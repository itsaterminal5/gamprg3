﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using System.Linq;

public class BuffElementController : MonoBehaviour {
    public Buff Buff;
    public Text Text;

    private void Start()
    {
        Assert.IsNotNull(Buff, "Buff is null");
        Assert.IsNotNull(Text, "Text is null");
    }

    private void Update()
    {
        string displayText = Buff.Name;
        float duration = Buff.TimeRemaining;

        // Display stacks if buff stacks with intensity
        if (Buff.Application == BuffApplication.StackingIntensity)
        {
            int stacks = Buff.Target.GetStacks(Buff.Id);
            displayText += "\tStacks: " + stacks;

            // Display the buff with highest duration (performance could be improved)
            duration = Buff.Target.GetBuffs(Buff.Id)
                .OrderByDescending(b => b.TimeRemaining).First()
                .TimeRemaining;
            Debug.Log(duration);
        }

        // Duration
        displayText += "\tDuration: " + duration.ToString("F0") + "s";

        Text.text = displayText;
    }
}

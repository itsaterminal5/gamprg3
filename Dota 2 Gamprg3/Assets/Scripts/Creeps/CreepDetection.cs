﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepDetection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Factions>().unitFaction != GetComponentInParent<Creep>().creep.faction)
        {
            GetComponentInParent<Creep>().enemylist.Add(other.gameObject);
            GetComponentInParent<Creep>().currentCreepState = Creep.CreepState.Chase;
        }
        
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Factions>().unitFaction != GetComponentInParent<Creep>().creep.faction)
        {
            GetComponentInParent<Creep>().enemylist.Remove(other.gameObject);
        }
        

    }

}

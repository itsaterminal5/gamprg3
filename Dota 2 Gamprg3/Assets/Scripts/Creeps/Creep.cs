﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum CreepType { Melee, Ranged, Siege };
public class Creep : MonoBehaviour
{
    public NavMeshAgent creepAgent;

    public Unit creep = new Unit();
    public List<GameObject> enemylist = new List<GameObject>();
    public GameObject currentEnemy;

    public Transform[] waypoints;
    public Vector3 destPos;
    Vector3 dir = new Vector3(0.0f, 0.0f, 0.0f);
    public Transform targetTransform;
    public float curSpeed;
    public float rotationSpeed;

    public LanePos unitLane;
    public int index=0;
    public CreepType type;
    public bool isSuper;
    public float displayHealth;

    public float chaseRange = 7.5f;
    //public Transform currentTarget;
    private float timeLastAttacked;
    private float atkInterval;
    public GameObject projectile;

    private int bonusHealth;
    private int bonusAtk;
    
    public enum CreepState
    {
        FollowLane,
        Chase,
        Attack,
        Dead
    }
    public CreepState currentCreepState;
    // Start is called before the first frame update
    void Start()
    {
        curSpeed = 5f;
        rotationSpeed = 3f;
        
        destPos = transform.position;
        creepAgent = gameObject.GetComponent<NavMeshAgent>();
        
        SetStats();
    }

    // Update is called once per frame
    void Update()
    {
        displayHealth = creep.currentHealth;
        UpdateCreepState();
        creep.healthRegen = 0.5f;
        
        creep.regenHealth(0);
        

        if (Input.GetKeyDown(KeyCode.A))
        {
            currentCreepState = CreepState.Attack;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            currentCreepState = CreepState.FollowLane;
        }
        
        CheckIfAlive();
        
    }

    public void SetStats()
    {
        
        if (type == CreepType.Melee)
        {
            if (!isSuper)
            {
                bonusHealth = 12;
                bonusAtk = 1;
                atkInterval = 1;
                creep.health = 550 + (bonusHealth * GameManager.instance.creepStatMultiplier);
                creep.atkDmg = 21 + (bonusAtk * GameManager.instance.creepStatMultiplier);
            }
            else
            {
                bonusHealth = 19;
                bonusAtk = 2;
                atkInterval = 1;
                creep.health = 700 + (bonusHealth * GameManager.instance.creepStatMultiplier);
                creep.atkDmg = 40 + (bonusAtk * GameManager.instance.creepStatMultiplier);
            }
            
        }
        else if (type == CreepType.Ranged)
        {
            if (!isSuper)
            {
                bonusHealth = 12;
                bonusAtk = 2;
                atkInterval = 1;
                creep.health = 300 + (bonusHealth * GameManager.instance.creepStatMultiplier);
                creep.atkDmg = 24 + (bonusAtk * GameManager.instance.creepStatMultiplier);
            }
            else
            {
                bonusHealth = 18;
                bonusAtk = 3;
                atkInterval = 1;
                creep.health = 475 + (bonusHealth * GameManager.instance.creepStatMultiplier);
                creep.atkDmg = 44 + (bonusAtk * GameManager.instance.creepStatMultiplier);
            }
        }
        else if (type == CreepType.Siege)
        {
            if (!isSuper)
            {
                creep.health = 875;
                creep.atkDmg = 41;
                atkInterval = 3;
            }
            else
            {
                creep.health = 875;
                creep.atkDmg = 57;
                atkInterval = 3;
            }
        }
        creep.currentHealth = creep.health;
        creep.faction = GetComponent<Factions>().unitFaction;
    }

    public void UpdateCreepState()
    {
        switch(currentCreepState)
        {
            case CreepState.FollowLane:
                FollowCurrentLane();
                break;
            case CreepState.Chase:
                ChaseTarget();
                break;
            case CreepState.Attack:
                Attack();
                break;
            case CreepState.Dead:
                Death();
                break;

        }
    }

    public void FollowCurrentLane()
    {
        creepAgent.isStopped = false;
        //Debug.Log(Vector3.Distance(transform.position, destPos));
        if (Vector3.Distance(transform.position, destPos) <= 4f)
        {
            //Debug.Log("Moving");
            FindNextPoint();
        }
        //else if (Vector3.Distance(transform.position, targetTransform.position) <= 5f)
        //{
        //    currentCreepState = CreepState.Chase;
        //}


        creepAgent.SetDestination(destPos);

        //dir = destPos - transform.position;
        //dir += AvoidObstacles();
        //dir.Normalize();
        //Quaternion targetRotation = Quaternion.LookRotation(dir);
        //transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);

        //transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);
        
    }

    public bool IsInCurrentRange(Vector3 pos)
    {
        float xPos = Mathf.Abs(pos.x - transform.position.x);
        float zPos = Mathf.Abs(pos.z - transform.position.z);

        if (xPos <= 1 && zPos <= 1)
        {
            return true;
        }

        return false;
    }

    public void FindNextPoint()
    {
        //int index = 0;
        //index++;
        if (index>=waypoints.Length)
        {
            curSpeed = 0;
            rotationSpeed = 0f;
        }
        float radius = 5.0f;
        Vector3 rndPosition = Vector3.zero;
        destPos = waypoints[index].transform.position + rndPosition;
        //Debug.Log(index);
        //Debug.Log(IsInCurrentRange(destPos));
        if (IsInCurrentRange(destPos))
        {
            index++;
            if (index>=waypoints.Length)
            {
                Destroy(gameObject);
            }
            //Debug.Log(index);
            rndPosition = new Vector3(Random.Range(-radius, radius), 0.0f, Random.Range(-radius, radius));
            destPos = waypoints[index].transform.position + rndPosition;
        }
    }

    public void ChaseTarget()
    {
        creepAgent.isStopped = false;

        currentEnemy = enemylist[0];

        if (enemylist.Count <= 0)
        {
            currentCreepState = CreepState.FollowLane;
        }

        
        if (currentEnemy != null)
        {
            destPos = currentEnemy.transform.position;
            creepAgent.SetDestination(destPos);
            DetermineAttackDistance();
        }
        else 
        {
            RemoveDead();
            currentEnemy = enemylist[0];
        }

        


    }


    public void DetermineAttackDistance()
    {
        if (type == CreepType.Melee)
        {
            
            if (Vector3.Distance(transform.position, currentEnemy.transform.position) <= 3f)
            {
                currentCreepState = CreepState.Attack;             
            }
            else
            {
                currentCreepState = CreepState.Chase;
            }
        }
        else if (type == CreepType.Ranged)
        {
            if (Vector3.Distance(transform.position, currentEnemy.transform.position) <= 5f)
            {
                currentCreepState = CreepState.Attack;
            }
            else
            {
                currentCreepState = CreepState.Chase;
            }
        }
        else if (type == CreepType.Siege)
        {
            if (Vector3.Distance(transform.position, currentEnemy.transform.position) <= 6.9f)
            {
                currentCreepState = CreepState.Attack;
            }
            else
            {
                currentCreepState = CreepState.Chase;
            }
        }
    }

    public void Attack()
    {
        creepAgent.isStopped = true;

        if (enemylist.Count <= 0)
        {
            currentCreepState = CreepState.FollowLane;
        }

        if (type == CreepType.Melee)
        {
            MeleeAttack();
        }
        else
        {
            RangedAttack();
        }
        

    }

   public void MeleeAttack()
   {
        if (currentEnemy != null)
        {
            if (Time.time - timeLastAttacked >= atkInterval)
            {
                if (currentEnemy.GetComponent<Creep>() != null)
                {
                    currentEnemy.GetComponent<Creep>().creep.TakeDamage(creep.atkDmg);
                }
                else if (currentEnemy.GetComponent<Tower>() != null)
                {
                    currentEnemy.GetComponent<Tower>().tower.TakeStructureDamage(creep.atkDmg);
                    
                }
                else if (currentEnemy.GetComponent<Barracks>() != null)
                {
                    currentEnemy.GetComponent<Barracks>().barracks.TakeStructureDamage(creep.atkDmg);

                }
                else if (currentEnemy.GetComponent<Ancient>() != null)
                {
                    currentEnemy.GetComponent<Ancient>().ancient.TakeStructureDamage(creep.atkDmg);

                }

                timeLastAttacked = Time.time;
            }
        }
        else if (enemylist.Count>0)
        {
            RemoveDead();
            currentEnemy = enemylist[0];
            currentCreepState = CreepState.Chase;
        }
        else
        {
            currentCreepState = CreepState.FollowLane;
        }
    }

   public void RangedAttack()
   {
        if (currentEnemy != null)
        {
            if (Time.time - timeLastAttacked >= atkInterval)
            {
                Shoot();

                timeLastAttacked = Time.time;
            }
        }
        else
        {
            RemoveDead();
            currentEnemy = enemylist[0];
            currentCreepState = CreepState.Chase;
        }
    }

    public void Shoot()
    {
        //Debug.Log("shooting " + currentTarget);
        GameObject rangedProjectile = Instantiate(projectile,transform.position, transform.rotation);
        rangedProjectile.transform.SetParent(gameObject.transform);
        CreepProjectile seekingBullet = rangedProjectile.GetComponent<CreepProjectile>();
        if (seekingBullet != null)
        {
            seekingBullet.Seek(currentEnemy.transform);
        }
    }

    public void RemoveDead()
    {
        foreach (GameObject enemy in enemylist)
        {
            if (enemy == null)
            {
                enemylist.Remove(enemy);
            }
        }

    }

    public void CheckIfAlive()
    {
        if (creep.currentHealth <= 0)
        {
            currentCreepState = CreepState.Dead;
        }
    }

    public void Death()
    {
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepProjectile : MonoBehaviour
{
    private Transform target;
    public float speed = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distance = speed * Time.deltaTime;

        if (dir.magnitude <= distance)
        {
            HitTarget();
            return;
        }
        transform.Translate(dir.normalized * distance, Space.World);
    }
    public void Seek(Transform targetToSeek)
    {
        target = targetToSeek;
    }

    private void HitTarget()
    {
        //Debug.Log("dealth " + GetComponentInParent<Tower>().tower.atkDmg);

        if (target.gameObject.GetComponent<Factions>().unitFaction != GetComponentInParent<Creep>().creep.faction)
        {
            if (target.GetComponent<Creep>()!=null)
            {
                target.GetComponent<Creep>().creep.TakeDamage(GetComponentInParent<Creep>().creep.atkDmg);
                if (target.GetComponent<Creep>().creep.currentHealth <= 0)
                {
                    GetComponentInParent<Creep>().enemylist.Remove(target.gameObject);
                }
                
            }
            else if (target.GetComponent<Tower>() != null)
            {
                target.GetComponent<Tower>().tower.TakeStructureDamage(GetComponentInParent<Creep>().creep.atkDmg);
                if (target.GetComponent<Tower>().tower.currentHealth <= 0)
                {
                    GetComponentInParent<Creep>().enemylist.Remove(target.gameObject);
                }
                
            }
            else if (target.GetComponent<Barracks>() != null)
            {
                target.GetComponent<Barracks>().barracks.TakeStructureDamage(GetComponentInParent<Creep>().creep.atkDmg);
                if (target.GetComponent<Barracks>().barracks.currentHealth <= 0)
                {
                    GetComponentInParent<Creep>().enemylist.Remove(target.gameObject);
                }
                
            }
            else if (target.GetComponent<Ancient>() != null)
            {
                target.GetComponent<Ancient>().ancient.TakeStructureDamage(GetComponentInParent<Creep>().creep.atkDmg);
                if (target.GetComponent<Ancient>().ancient.currentHealth <= 0)
                {
                    GetComponentInParent<Creep>().enemylist.Remove(target.gameObject);
                }
                
            }




            Destroy(gameObject);
        }
        //Debug.Log("Hit");
    }
}

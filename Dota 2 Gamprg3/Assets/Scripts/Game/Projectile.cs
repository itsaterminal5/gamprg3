﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Transform target;
    public float speed = 0.75f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distance = speed * Time.deltaTime;

        if (dir.magnitude<=distance)
        {
            HitTarget();
            return;
        }
        transform.Translate(dir.normalized * distance, Space.World);
    }
    public void Seek(Transform targetToSeek)
    {
        target = targetToSeek;
    }

    private void HitTarget()
    {
        //Debug.Log("dealth " + GetComponentInParent<Tower>().tower.atkDmg);
        
        if (target.gameObject.GetComponent<Factions>().unitFaction != GetComponentInParent<Tower>().tower.faction)
        {
            target.GetComponent<Creep>().creep.TakeDamage(GetComponentInParent<Tower>().tower.atkDmg);
            if (target.GetComponent<Creep>().creep.currentHealth <= 0)
            {
                GetComponentInParent<Tower>().enemylist.Remove(target.gameObject);
            }
            Destroy(gameObject);
        }
        //Debug.Log("Hit");
    }

    
}

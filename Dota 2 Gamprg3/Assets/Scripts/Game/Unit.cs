﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit 
{
    public float health;
    public float currentHealth;

    public float healthRegen;

    public float mana;
    public float currentMana;

    public float manaRegen;

    public float armor;
    
    public float moveSpeed;
    public float atkSpeed;

    public bool isRanged;

    

    public int bounty;
    public float atkDmg;

    public Faction faction;



    public void regenHealth(int strength)
    {
        if (currentHealth > 0)
        {
            currentHealth += (healthRegen + (strength * 0.1f)) * (Time.deltaTime * 0.1f);
            if (currentHealth >= health)
            {
                currentHealth = health;
            }
            else if (currentHealth <= 0)
            {
                currentHealth = 0;
            }
        }
        
    }

    public void regenMana(int intelligence)
    {
        if (currentHealth>0)
        {
            currentMana += (manaRegen + (intelligence * 0.1f)) * (Time.deltaTime * 0.1f);
            if (currentMana >= mana)
            {
                currentMana = mana;
            }
            else if (currentMana <= 0)
            {
                currentMana = 0;
            }
        }
        
    }

    public void TakeDamage(float dmgAmt)
    {
        currentHealth -= dmgAmt;
        
    }
    public void UseMana(float Amt)
    {
        currentMana -= Amt;
    }

    
}

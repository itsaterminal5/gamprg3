﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private float seconds;
    public int min;
    public int roundedSeconds;
    private float secondDisplay;
    private int roundedSecondDisplay;
    private bool hasAdvancedinMins;
    private bool colorHasChanged;
    public Text timeText;
    public Light skyLight;
    public Text DireVicotory;
    public Text RadVicotory;

    public GameObject playerHero;
    public GameObject comHero;

    public Button DebugButton;
    public GameObject debugPanel;

    public List<GameObject> radHeroes = new List<GameObject>();
    public List<Transform> radSpawn = new List<Transform>();

    public List<GameObject> direHeroes = new List<GameObject>();
    public List<Transform> direSpawn = new List<Transform>();
    

    public int creepStatMultiplier;
    bool bonusMultiplied;

    

    public int debugTimeMultiplier;
    public bool gameStarted;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DireVicotory.enabled = false;
        RadVicotory.enabled = false;
        debugPanel.SetActive(false);
        debugTimeMultiplier = 1;
        foreach (Transform spawn in direSpawn)
        {
            Instantiate(comHero, spawn.transform.position, transform.rotation);
        }
        for (int i = 0; i < radSpawn.Count; i++)
        {
            if (i==2)
            {
                Instantiate(playerHero, radSpawn[i].transform.position, transform.rotation);
            }
            else
            {
                Instantiate(comHero, radSpawn[i].transform.position, transform.rotation);
            }
        }
        //gameStarted = false;
        //timeText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //if (gameStarted)
        //{
        //    TimeFlow();
        //}
        TimeFlow();
    }

    public void TimeFlow()
    {
        timeText.enabled = true;
        seconds += Time.deltaTime*debugTimeMultiplier;

        roundedSeconds = (int)seconds;
        DisplayTime();

        if (roundedSeconds > 0 && roundedSeconds % 60 == 0)
        {
            if (!hasAdvancedinMins)
            {
                min++;
                hasAdvancedinMins = true;
            }
        }
        else
        {
            hasAdvancedinMins = false;
        }
        ChangeDayLight();
        CheckStatBonus();
    }

    public void DisplayTime()
    {
        secondDisplay += Time.deltaTime*debugTimeMultiplier;
        roundedSecondDisplay = (int)secondDisplay;
        if (roundedSecondDisplay > 9)
        {
            timeText.text = min.ToString() + ":" + roundedSecondDisplay.ToString();
        }
        else
        {
            timeText.text = min.ToString() + ":0" + roundedSecondDisplay.ToString();
        }
        if (roundedSecondDisplay >= 60)
        {
            secondDisplay = 0;
        }
    }
    public void ChangeDayLight()
    {
        if (roundedSeconds > 0 && roundedSeconds % 300 == 0)
        {
            if (!colorHasChanged)
            {
                if (skyLight.enabled)
                {
                    skyLight.enabled = false;
                }
                else
                {
                    skyLight.enabled = true;
                }
                colorHasChanged = true;
            }

        }
        else
        {
            colorHasChanged = false;
        }
    }

    public void CheckStatBonus()
    {
        
        if (roundedSeconds > 0 && roundedSeconds % 450 == 0)
        {
            if (!bonusMultiplied)
            {
                creepStatMultiplier++;
                //Debug.Log(creepStatMultiplier);
                bonusMultiplied = true;
            }
        }
    }



    public void ActivateDebugPanel()
    {
        debugPanel.SetActive(true);
    }
    public void ExitDebug()
    {
        debugPanel.SetActive(false);
    }
}

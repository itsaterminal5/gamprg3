﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public Image hpBar;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<Creep>()!=null)
        {
            hpBar.fillAmount = GetComponentInParent<Creep>().creep.currentHealth / GetComponentInParent<Creep>().creep.health;
            transform.LookAt(Camera.main.transform);
            transform.Rotate(0, 180, 0);
        }
        else if (GetComponentInParent<Tower>() != null)
        {
            hpBar.fillAmount = GetComponentInParent<Tower>().tower.currentHealth / GetComponentInParent<Tower>().tower.health;
            transform.LookAt(Camera.main.transform);
            transform.Rotate(0, 180, 0);
        }

        else if (GetComponentInParent<Barracks>() != null)
        {
            hpBar.fillAmount = GetComponentInParent<Barracks>().barracks.currentHealth / GetComponentInParent<Barracks>().barracks.health;
            transform.LookAt(Camera.main.transform);
            transform.Rotate(0, 180, 0);
        }

        else if (GetComponentInParent<Tower>() != null)
        {
            hpBar.fillAmount = GetComponentInParent<Ancient>().ancient.currentHealth / GetComponentInParent<Ancient>().ancient.health;
            transform.LookAt(Camera.main.transform);
            transform.Rotate(0, 180, 0);
        }

        if (hpBar.fillAmount <=0)
        {
            gameObject.SetActive(false);
        }


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug : MonoBehaviour
{
    public List<GameObject> allBarracks = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void DestroyAllBarracks()
    {
        foreach (GameObject barracks in allBarracks)
        {
            if (barracks.GetComponent<Barracks>()!=null)
            {
                barracks.GetComponent<Barracks>().precedingTower = null;
                barracks.GetComponent<Barracks>().barracks.isInvunerable = false;
                barracks.GetComponent<Barracks>().barracks.TakeStructureDamage(3000);
            }
            
        }
    }

    public void multiplyGameFive()
    {
        GameManager.instance.debugTimeMultiplier = 5;
    }

    public void multiplyGameTen()
    {
        GameManager.instance.debugTimeMultiplier = 10;
    }

    public void multiplyGameNormal()
    {
        GameManager.instance.debugTimeMultiplier = 1;
    }


}

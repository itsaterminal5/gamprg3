﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ancient : MonoBehaviour
{
    public Structure ancient = new Structure();
    public float displayHealth;
    public bool isInvincible;
    public List<GameObject> t4Towers = new List<GameObject>();
    //public Faction thisFaction;
    // Start is called before the first frame update
    void Start()
    {
        setStats();
    }

    // Update is called once per frame
    void Update()
    {
        
        foreach (GameObject tower in t4Towers)
        {
            if (tower==null)
            {
                t4Towers.Remove(tower);
            }
        }

        if (t4Towers.Count>0)
        {
            ancient.isInvunerable = true;
        }
        else
        {
            ancient.isInvunerable = false;
        }

        DetermineVictor();
        isInvincible = ancient.isInvunerable;
        displayHealth = ancient.currentHealth;
        //thisFaction = ancient.faction;
    }
    public void setStats()
    {
        ancient.health = 4500;
        ancient.currentHealth = ancient.health;
        displayHealth = ancient.currentHealth;
        ancient.isInvunerable = true;
        ancient.faction = GetComponent<Factions>().unitFaction;
        ancient.armor = 13;
        //thisFaction = ancient.faction;
    }

    public void DetermineVictor()
    {
        if (ancient.faction == Faction.Dire)
        {
            if (ancient.currentHealth <= 0)
            {
                GameManager.instance.RadVicotory.enabled = true;
                GameManager.instance.gameStarted = false;
                
            }
        }
        else
        {
            if (ancient.currentHealth <= 0)
            {
                GameManager.instance.DireVicotory.enabled = true;
                GameManager.instance.gameStarted = false;
                
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : MonoBehaviour
{
    public Structure barracks = new Structure();
    
    public string type;
    public int creepWave;

    private MeshRenderer barracksRenderer;

    private BoxCollider barracksCollider;

    public bool superCreepsCanSpawn;

    public Barracks enemyBarracks;

    public Transform spawnPoint;

    public GameObject radiantRangeCreep;
    public GameObject radiantMeleeCreep;
    public GameObject radiantSiegeCreep;

    public GameObject superRadiantRangeCreep;
    public GameObject superRadiantMeleeCreep;
    public GameObject superRadiantSiegeCreep;


    public GameObject direRangeCreep;
    public GameObject direMeleeCreep;
    public GameObject direSiegeCreep;

    public GameObject superDireRangeCreep;
    public GameObject superDireMeleeCreep;
    public GameObject superDireSiegeCreep;



    public bool hasSpawnedCreep;
    public GameObject precedingTower;


    //display test
    public float healthDisplay;
    public Faction thisFaction;
    public LanePos structureLane;
    // Start is called before the first frame update
    void Start()
    {
        barracksRenderer = GetComponent<MeshRenderer>();
        barracksCollider = GetComponent<BoxCollider>();
        
        SetStats();
    }

    // Update is called once per frame
    void Update()
    {
        CreepWave();
        healthDisplay = barracks.currentHealth;
        if (precedingTower != null )
        {
            barracks.isInvunerable = true;
        }
        
        RemoveStructure();
    }

    public void SetStats()
    {
        if (type=="Melee")
        {
            barracks.health = 2200;
            barracks.armor = 15;
        }
        else
        {
            barracks.health = 1300;
            barracks.armor = 9;
            
        }
        barracks.currentHealth = barracks.health;
        barracks.faction = GetComponent<Factions>().unitFaction;
        thisFaction = barracks.faction;
        barracks.lane = GetComponent<Lane>().lanePos;
        structureLane = barracks.lane;
        superCreepsCanSpawn = false;
    }


    public void CreepWave()
    {
        if (GameManager.instance.roundedSeconds > 0 && GameManager.instance.roundedSeconds % 30 == 0)
        {
            if (!hasSpawnedCreep)
            {
                DetermineCreepType();

                hasSpawnedCreep = true;
            }
        }
        else
        {
            hasSpawnedCreep = false;
        }
    }

    public void DetermineCreepType()
    {
        if (enemyBarracks.barracks.currentHealth<=0)
        {
            superCreepsCanSpawn = true;
        }

        if (superCreepsCanSpawn)
        {
            spawnSuperCreep();
        }
        else
        {
            spawnCreep();
        }
    }

    public void spawnCreep()
    {
        creepWave++;
        if (barracks.faction == Faction.Radiant)
        {
            if (type == "Melee")
            {
                for (int i = 0; i < 3; i++)
                {
                    GameObject mCreep = (GameObject)Instantiate(radiantMeleeCreep, spawnPoint.transform.position, transform.rotation);
                    mCreep.transform.SetParent(spawnPoint.transform);
                    mCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    mCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                    

                }
            }
            else 
            {
                GameObject rCreep = Instantiate(radiantRangeCreep, spawnPoint.transform.position, transform.rotation);
                rCreep.transform.SetParent(spawnPoint.transform);
                rCreep.GetComponent<Creep>().unitLane = barracks.lane;
                rCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                if (creepWave > 0 && creepWave % 10 == 0)
                {
                    GameObject sCreep = Instantiate(radiantSiegeCreep, spawnPoint.transform.position, transform.rotation);
                    sCreep.transform.SetParent(spawnPoint.transform);
                    sCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    sCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                }
                
                

            }
        }
        else if (barracks.faction == Faction.Dire)
        {
            if (type == "Melee")
            {
                for (int i = 0; i < 3; i++)
                {
                    GameObject mCreep = Instantiate(direMeleeCreep, spawnPoint.transform.position, transform.rotation);
                    mCreep.transform.SetParent(spawnPoint.transform);
                    mCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    mCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                }
            }
            else 
            {
                GameObject rCreep = Instantiate(direRangeCreep, spawnPoint.transform.position, transform.rotation);
                rCreep.transform.SetParent(spawnPoint.transform);
                rCreep.GetComponent<Creep>().unitLane = barracks.lane;
                rCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                if (creepWave > 0 && creepWave % 10 == 0)
                {
                    GameObject sCreep = Instantiate(direSiegeCreep, spawnPoint.transform.position, transform.rotation);
                    sCreep.transform.SetParent(spawnPoint.transform);
                    sCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    sCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                }
            }
        }
    }


    public void spawnSuperCreep()
    {
        creepWave++;
        if (barracks.faction == Faction.Radiant)
        {
            if (type == "Melee")
            {
                for (int i = 0; i < 3; i++)
                {
                    GameObject sMCreep = (GameObject)Instantiate(superRadiantMeleeCreep, spawnPoint.transform.position, transform.rotation);
                    sMCreep.transform.SetParent(spawnPoint.transform);
                    sMCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    sMCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;


                }
            }
            else
            {
                GameObject sRCreep = Instantiate(superRadiantRangeCreep, spawnPoint.transform.position, transform.rotation);
                sRCreep.transform.SetParent(spawnPoint.transform);
                sRCreep.GetComponent<Creep>().unitLane = barracks.lane;
                sRCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                if (creepWave > 0 && creepWave % 10 == 0)
                {
                    GameObject sSCreep = Instantiate(superRadiantSiegeCreep, spawnPoint.transform.position, transform.rotation);
                    sSCreep.transform.SetParent(spawnPoint.transform);
                    sSCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    sSCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                }



            }
        }
        else if (barracks.faction == Faction.Dire)
        {
            if (type == "Melee")
            {
                for (int i = 0; i < 3; i++)
                {
                    GameObject sMCreep = Instantiate(superDireMeleeCreep, spawnPoint.transform.position, transform.rotation);
                    sMCreep.transform.SetParent(spawnPoint.transform);
                    sMCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    sMCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                }
            }
            else
            {
                GameObject sRCreep = Instantiate(superDireRangeCreep, spawnPoint.transform.position, transform.rotation);
                sRCreep.transform.SetParent(spawnPoint.transform);
                sRCreep.GetComponent<Creep>().unitLane = barracks.lane;
                sRCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                if (creepWave > 0 && creepWave % 10 == 0)
                {
                    GameObject sSCreep = Instantiate(superDireSiegeCreep, spawnPoint.transform.position, transform.rotation);
                    sSCreep.transform.SetParent(spawnPoint.transform);
                    sSCreep.GetComponent<Creep>().unitLane = barracks.lane;
                    sSCreep.GetComponent<Creep>().waypoints = GetComponent<Lane>().laneWaypoints;
                }
            }
        }
    }

    public void RemoveStructure()
    {
        barracks.DestroyStructure();
        if (barracks.isDestroyed)
        {
            //gameObject.SetActive(false);
            barracksRenderer.enabled = false;
            barracksCollider.enabled = false;
            
        }
    }
    
}

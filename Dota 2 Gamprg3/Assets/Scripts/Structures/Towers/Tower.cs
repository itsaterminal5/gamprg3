﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public Structure tower = new Structure();
    public List<GameObject> enemylist = new List<GameObject>();
    
    public Transform currentTarget;
    public float range = 5f;
    public int tier;
    public float fireRate = 1;
    public float fireCountdown = 0;
    public GameObject projectile;
    public GameObject precedingTier;
    public Transform firePoint;


    //Test
    public float healthDisplay;
    public bool isInvincible;
    // Start is called before the first frame update
    void Start()
    {
        tower.faction = GetComponent<Factions>().unitFaction;
        SetStats();
    }

    // Update is called once per frame
    void Update()
    {
        healthDisplay = tower.currentHealth;
        tier = Mathf.Clamp(tier, 1, 4);
        if (precedingTier!=null && tier < 4)
        {
            tower.isInvunerable = true;
        }
        else
        {
            tower.isInvunerable = false;
        }
        
        isInvincible = tower.isInvunerable;
        UpdateTarget();
        RemoveTower();
    }

    public void SetStats()
    {
        if (tier == 1)
        {
            tower.health = 1800;
            tower.armor = 12;
            tower.atkDmg = 110;
            fireRate = 1;
        }
        else if (tier == 2)
        {
            tower.health = 2000;
            tower.armor = 16;
            tower.atkDmg = 175;
            fireRate = 1.05f;

        }
        else if (tier == 3)
        {
            tower.health = 2000;
            tower.armor = 16;
            tower.atkDmg = 175;
            fireRate = 1.05f;

        }
        else if (tier == 4)
        {
            tower.health = 2100;
            tower.armor = 21;
            tower.atkDmg = 175;
            fireRate = 1.05f;
        }
        tower.currentHealth = tower.health;
    }

    public void RemoveDead()
    {
        foreach (GameObject enemy in enemylist)
        {
            if (enemy == null)
            {
                enemylist.Remove(enemy);
            }
        }
        
    }

    public void UpdateTarget()
    {
        RemoveDead();
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        
        foreach (GameObject curEnemy in enemylist)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, curEnemy.transform.position);
            if (distanceToEnemy<shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = curEnemy;
            }
        }
        if (nearestEnemy!=null&& shortestDistance<=range)
        {
            currentTarget = nearestEnemy.transform;
            if (fireCountdown<=0f)
            {
                Shoot();
                fireCountdown = 1f / fireRate;
            }
            fireCountdown -= Time.deltaTime;
        }
        else
        {
            currentTarget = null;
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    public void Shoot()
    {
        //Debug.Log("shooting " + currentTarget);
        GameObject towerBullet = Instantiate(projectile, firePoint.transform.position, firePoint.rotation);
        towerBullet.transform.SetParent(firePoint.transform);
        Projectile seekingBullet = towerBullet.GetComponent<Projectile>();
        if (seekingBullet!=null)
        {
            seekingBullet.Seek(currentTarget);
        }
    }
    
    public void RemoveTower()
    {
        tower.DestroyStructure();
        if (tower.isDestroyed)
        {
            //gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

}

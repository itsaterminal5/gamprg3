﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDetection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Factions>().unitFaction != GetComponentInParent<Tower>().tower.faction)
        {
            //Debug.Log(other.gameObject.name+"is an enemy");
            GetComponentInParent<Tower>().enemylist.Add(other.gameObject);

        }
        //else
        //{
        //    //Debug.Log(other.gameObject.name + "is an ally");
        //}
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Factions>().unitFaction != GetComponentInParent<Tower>().tower.faction||other.gameObject == null)
        {
            
            GetComponentInParent<Tower>().enemylist.Remove(other.gameObject);
        }
        
    }
}

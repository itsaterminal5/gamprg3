﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Structure : Unit
{
    
    public bool isInvunerable;
    public LanePos lane;
    public bool isDestroyed;
    //public Faction faction;

    
    public void TakeStructureDamage(float damage)
    {
        if (!isInvunerable)
        {
            currentHealth -= damage;
        }
        
    }

    public void DestroyStructure()
    {
        if (currentHealth<=0)
        {
            isDestroyed = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalMaiden : MonoBehaviour
{
    public Hero crystalMaiden = new Hero();
    public float displayHealth;
    public float displayMana;
    // Start is called before the first frame update
    void Start()
    {
        SetStats();
    }

    // Update is called once per frame
    void Update()
    {
        
        //if (Input.GetKeyDown(KeyCode.C))
        //{
        //    crystalMaiden.TakeDamage(50);
        //}
        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    crystalMaiden.UseMana(10);
        //}
        crystalMaiden.regenHealth(crystalMaiden.strength);
        crystalMaiden.regenMana(crystalMaiden.inteligence);
        displayHealth = crystalMaiden.currentHealth;
        displayMana = crystalMaiden.currentMana;
    }
    public void SetStats()
    {
        crystalMaiden.strength = 18;
        crystalMaiden.inteligence = 14;
        crystalMaiden.health = 200;
        crystalMaiden.currentHealth = crystalMaiden.health;
        crystalMaiden.healthRegen = 0;
        crystalMaiden.mana = 75;
        crystalMaiden.currentMana = crystalMaiden.mana;
        crystalMaiden.manaRegen = 1;
    }
}

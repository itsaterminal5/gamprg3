﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{


    public NavMeshAgent playerAgent;

    // Start is called before the first frame update
    void Start()
    {
        playerAgent = gameObject.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit , Mathf.Infinity))
            {
                
                playerAgent.SetDestination(hit.point);
                
            }
            
            
        }
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slardar : MonoBehaviour
{
    public Hero slardar = new Hero();
    public float displayHealth;
    public float displayMana;
    // Start is called before the first frame update
    void Start()
    {
        SetStats();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            slardar.TakeDamage(50);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            slardar.UseMana(10);
        }
        slardar.regenHealth(slardar.strength);
        slardar.regenMana(slardar.inteligence);
        displayHealth = slardar.currentHealth;
        displayMana = slardar.currentMana;
    }

    public void SetStats()
    {
        slardar.strength = 21;
        slardar.inteligence = 15;
        slardar.health = 200;
        slardar.currentHealth = slardar.health;
        slardar.healthRegen = 0.5f;
        slardar.mana = 75;
        slardar.currentMana = slardar.mana;
        slardar.manaRegen = 0;
    }
}
